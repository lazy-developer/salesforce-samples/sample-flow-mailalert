# 本リポジトリについて

## 概要

商談（Opportunity）が IsWon でクローズされた際に、特定の公開グループに対してメールを送信する、レコードトリガーフローのサンプル実装です。

## リンク

ブログにて、詳細を記載しています。
https://lazy-developer.jp/2022/12/04/salesforce-flow-mailalert/